import 'package:auge/environment/detail_environment.dart';
import 'package:auge/environment/models/environment_model.dart';
import 'package:flutter/material.dart';

class EnvironmentRow extends StatelessWidget {
  final Environment environment;
  const EnvironmentRow(
    this.environment, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            Row(
              children: [
                Text(environment.nome),
                Spacer(),
                GestureDetector(
                  child: Icon(Icons.arrow_forward),
                  onTap: () => Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DetailEnvironment(environment),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
