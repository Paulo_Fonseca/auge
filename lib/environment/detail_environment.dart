import 'package:auge/environment/models/environment_model.dart';
import 'package:flutter/material.dart';

class DetailEnvironment extends StatelessWidget {
  final Environment environment;
  const DetailEnvironment(this.environment, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          environment.nome,
          style: TextStyle(fontSize: 24),
        ),
      ),
      body: Column(
        children: [
          SizedBox(height: 15),
          Container(
            width: 400,
            margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(width: 1, color: Colors.grey)),
            ),
            child: Row(
              children: [
                Text(
                  'Último acesso:',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Spacer(),
                Text(
                  '20/12/1998 19:20',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: 400,
            margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
            padding: EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
              border: Border(bottom: BorderSide(width: 1, color: Colors.grey)),
            ),
            child: Row(
              children: [
                Text(
                  'Listar usuários com acesso ao ambiente',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    //TODO implementar próxima tela
                  },
                  child: Icon(Icons.arrow_forward_ios_rounded),
                )
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () => _show(context),
            child: Container(
              width: 350,
              alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
              decoration: BoxDecoration(
                color: Colors.green[400],
                border: Border.all(width: 1, color: Colors.grey),
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
              child: Text(
                'Fazer acesso',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          SizedBox(height: 40)
        ],
      ),
    );
  }

  void _show(BuildContext ctx) {
    showModalBottomSheet(
      isScrollControlled: true,
      isDismissible: false,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
      ),
      context: ctx,
      builder: (ctx) => Padding(
        padding: EdgeInsets.only(
          bottom: 24,
          top: 16,
          right: 16,
          left: 16,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 15),
            Text(
              "Acessar ambiente",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 15),
            Text(
              "Deseja fazer acesso ao *NOME DO AMBIENTE*, no dia 15/06/2021 às 20:00",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 16),
            ),
            SizedBox(height: 15),
            GestureDetector(
              //TODO IMPLEMENTAR ACESSO
              onTap: () => null,
              child: Container(
                width: 350,
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                decoration: BoxDecoration(
                  color: Colors.green[400],
                  border: Border.all(width: 1, color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                child: Text(
                  'Acessar',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            SizedBox(height: 15),
            GestureDetector(
              onTap: () => Navigator.pop(ctx),
              child: Container(
                width: 350,
                alignment: Alignment.center,
                margin: EdgeInsets.fromLTRB(15, 10, 15, 0),
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Text(
                  'Cancelar',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.green[600],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
