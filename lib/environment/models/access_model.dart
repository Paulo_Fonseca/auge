// To parse this JSON data, do
//
//     final access = accessFromJson(jsonString);

import 'dart:convert';

Access accessFromJson(String str) => Access.fromJson(json.decode(str));

String accessToJson(Access data) => json.encode(data.toJson());

class Access {
    Access({
        required this.id,
        required this.usuarioId,
        required this.ambienteId,
        required this.data,
    });

    final int id;
    final int usuarioId;
    final int ambienteId;
    final int data;

    factory Access.fromJson(Map<String, dynamic> json) => Access(
        id: json["id"],
        usuarioId: json["usuario_id"],
        ambienteId: json["ambiente_id"],
        data: json["data"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "usuario_id": usuarioId,
        "ambiente_id": ambienteId,
        "data": data,
    };
}
