import 'dart:convert';

Environment environmentFromJson(String str) =>
    Environment.fromJson(json.decode(str));

String environmentToJson(Environment data) => json.encode(data.toJson());

class Environment {
  Environment({
    required this.id,
    required this.nome,
    required this.vagas,
  });

  final int id;
  final String nome;
  final int vagas;

  factory Environment.fromJson(Map<String, dynamic> json) => Environment(
        id: json["id"],
        nome: json["nome"],
        vagas: json["vagas"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nome": nome,
        "vagas": vagas,
      };
}
