import 'package:auge/environment/environment_row_view.dart';
import 'package:auge/environment/models/environment_model.dart';
import 'package:auge/mocks/mocks.dart';
import 'package:flutter/material.dart';

class EnvironmentView extends StatelessWidget {
  const EnvironmentView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Environment> environments = Mocks.environments;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          "Ambientes",
          style: TextStyle(fontSize: 24),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        child: SingleChildScrollView(
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  //TODO implementar metodo de busca
                },
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Pesquisar ambiente"),
                      Icon(Icons.search),
                    ],
                  ),
                  height: 38,
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                ),
              ),
              SizedBox(height: 16),
              Column(
                  children: [...environments.map((e) => EnvironmentRow(e))])
            ],
          ),
        ),
      ),
    );
  }
}
