import 'package:auge/environment/models/access_model.dart';
import 'package:auge/environment/models/environment_model.dart';

class Mocks {
  static List<Environment> environments = [
    Environment.fromJson({
      "id": 1,
      "nome": "Ambiente 1",
      "vagas": 5,
    }),
    Environment.fromJson({
      "id": 2,
      "nome": "Ambiente 2",
      "vagas": 10,
    }),
    Environment.fromJson({
      "id": 3,
      "nome": "Ambiente 3",
      "vagas": 15,
    }),
    Environment.fromJson({
      "id": 4,
      "nome": "Ambiente 4",
      "vagas": 20,
    }),
  ];

  static List<Access> access = [
    //Ambiente 1
    Access.fromJson({
      "id": 1,
      "usuarioId": 1,
      "ambienteId": 1,
      "data": 1623604807000
    }),
    Access.fromJson({
      "id": 2,
      "usuarioId": 4,
      "ambienteId": 1,
      "data": 1623607027000
    }),
    Access.fromJson({
      "id": 3,
      "usuarioId": 2,
      "ambienteId": 1,
      "data": 1623608407000
    }),
    Access.fromJson({
      "id": 4,
      "usuarioId": 3,
      "ambienteId": 1,
      "data": 1623597607000
    }),
    //Ambiente 2
    Access.fromJson({
      "id": 1,
      "usuarioId": 1,
      "ambienteId": 2,
      "data": 1623597607000
    }),
    Access.fromJson({
      "id": 2,
      "usuarioId": 4,
      "ambienteId": 2,
      "data": 1623607027000
    }),
    Access.fromJson({
      "id": 3,
      "usuarioId": 2,
      "ambienteId": 2,
      "data": 1623608407000
    }),
    Access.fromJson({
      "id": 4,
      "usuarioId": 3,
      "ambienteId": 2,
      "data": 1623604807000
    }),
  ];
}
